#include "ntc.h"
#include "adc.h"
#include "math.h"

float Get_Temperature()
{	
    float Temperature=0.0;
    float R2=0.0;
    float R1=10000.0;
    float T2=298.15;//273.15+25;
    float B=3950.0;
    float K=273.15;
    float R2V=0.0;
		HAL_ADC_Start(&hadc1);//����ADC1
		if(HAL_ADC_PollForConversion(&hadc1,200)==HAL_OK)
		{
			u16 ADC_Value=HAL_ADC_GetValue(&hadc1);
      R2V=( ADC_Value*(3.3/4096));    //12λADC
      R2=(R2V*R1)/(3.3-R2V);
      Temperature=1.0/(1.0/T2+log(R2/R1)/B)-K+0.5;
    }
    return Temperature;
}
	
