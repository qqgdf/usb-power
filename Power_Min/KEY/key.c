#include "key.h"
#include "main.h"
#include "ina219.h"
#include "lcd_init.h"
#include "lcd.h"
#include "ntc.h"
u8 hour=0,mintue=0,sec=0;
u16 time=0;
float mAH=0.0,mwh=0;
float Power_Max;
float Temperature_Max;
u8 Page_One=1;
u8 Page_Two=1;
u8 m=1;
void Get_Power_Max()
{
			if(Power_Max<INA219_Get_Power())
		{
			Power_Max=INA219_Get_Power();
		}
}


void Get_Temperature_Max()
{
			if(Temperature_Max<Get_Temperature())
		{
			Temperature_Max=Get_Temperature();
		}
}

void display()
{
		if(Page_One==1)
		{
				if(m)
				{
					LCD_Fill(0,0,LCD_W,LCD_H,BLACK);
					m=m-1;
				}
				 
			   
		LCD_ShowString(16,0,"U:",BROWN,BLACK,24,1);//��ʾ�ַ���
		LCD_ShowFloatNum(42,0, INA219_Get_BusVoltage(),5, BROWN ,BLACK,24);
		LCD_ShowChar(122,0,'V',BROWN,BLACK,24,1);
		
		//电流
		LCD_ShowString(16,24,"I:",BLUE,BLACK,24,1);//��ʾ�ַ���
		LCD_ShowFloatNum(42,24,INA219_Get_Current(),5,BLUE ,BLACK,24);
		LCD_ShowChar(122,24,'A',BLUE,BLACK,24,1);
		
    //功率
		LCD_ShowString(16,48,"P:",LIGHTGREEN,BLACK,24,1);//��ʾ�ַ���
		LCD_ShowFloatNum(42,48,INA219_Get_Power(),5,LIGHTGREEN,BLACK,24);
		LCD_ShowChar(122,48,'W',LIGHTGREEN,BLACK,24,1);

		}
	else if( Page_One==2)
	{
				if(m)
				{
					LCD_Fill(0,0,LCD_W,LCD_H,BLACK);
					m=m-1;
				}


			LCD_ShowChinese(0,8,"�����",BROWN,BLACK,16,1);
			LCD_ShowChar(64,8,':',BROWN,BLACK,16,1);
	    LCD_ShowFloatNum(84,8,Power_Max,5,BROWN,BLACK,16);
		  LCD_ShowChar(144,8,'W',BROWN,BLACK,16,1);
				
			LCD_ShowChinese(0,24,"����ʱ��", LGRAY,BLACK,16,1);
			LCD_ShowChar(64,24,':',LGRAY,BLACK,16,1);
 		  LCD_ShowIntNum(84,24,hour,2,LGRAY,BLACK,16);
		  LCD_ShowChar(101,24,':',LGRAY,BLACK,16,1);
		  LCD_ShowIntNum(109,24,mintue,2,LGRAY,BLACK,16);
		  LCD_ShowChar(126,24,':',LGRAY,BLACK,16,1);
		  LCD_ShowIntNum(134,24,sec,2,LGRAY,BLACK,16);
				
			LCD_ShowChinese(0,40,"�������",LIGHTGREEN,BLACK,16,1);
			LCD_ShowChar(64,40,':',LIGHTGREEN,BLACK,16,1);
			//LCD_ShowFloatNum(84,40,mAH,5,LIGHTGREEN,BLACK,16);
			LCD_ShowIntNum(84,40,mAH,4,LIGHTGREEN,BLACK,16);
		  LCD_ShowString(134,40,"mAH",LIGHTGREEN,BLACK,16,1);//��ʾ�ַ���
				
			LCD_ShowChinese(0,56,"����¶�",LGRAYBLUE,BLACK,16,1);
			LCD_ShowChar(64,56,':',LGRAYBLUE,BLACK,16,1);
			LCD_ShowIntNum(100,56,Temperature_Max,4,LGRAYBLUE,BLACK,16);
//			LCD_ShowFloatNum1(100,56,Temperature_Max,3,LGRAYBLUE,BLACK,16);
	}
	else if( Page_One==3)
	  {
				if(m)
				{
					LCD_Fill(0,0,LCD_W,LCD_H,BLACK);
					m=m-1;
				}

	
		LCD_ShowFloatNum(0,0, INA219_Get_BusVoltage(),5, BROWN ,BLACK,24);
		LCD_ShowChar(72,0,'V',BROWN,BLACK,24,1);
		LCD_ShowFloatNum(0,24,INA219_Get_Current(),5,BLUE ,BLACK,24);
		LCD_ShowChar(72,24,'A',BLUE,BLACK,24,1);
		LCD_ShowFloatNum(0,48,INA219_Get_Power(),5,LIGHTGREEN,BLACK,24);
		LCD_ShowChar(72,48,'W',LIGHTGREEN,BLACK,24,1);
				
				
			LCD_ShowIntNum(84,0,mAH,4,LIGHTGREEN,BLACK,16);
		  LCD_ShowString(134,0,"mAH",LIGHTGREEN,BLACK,16,1);//��ʾ�ַ���
			LCD_ShowIntNum(84,20,mwh,4,LIGHTGREEN,BLACK,16);
		  LCD_ShowString(134,20,"mWH",LIGHTGREEN,BLACK,16,1);//��ʾ�ַ���

 		  LCD_ShowIntNum(84,40,hour,2,LGRAY,BLACK,16);
		  LCD_ShowChar(101,40,':',LGRAY,BLACK,16,1);
		  LCD_ShowIntNum(109,40,mintue,2,LGRAY,BLACK,16);
		  LCD_ShowChar(126,40,':',LGRAY,BLACK,16,1);
		  LCD_ShowIntNum(134,40,sec,2,LGRAY,BLACK,16);
			LCD_ShowIntNum(120,60,Get_Temperature(),2,LGRAYBLUE,BLACK,16);
//			LCD_ShowFloatNum1(120,60,Get_Temperature(),3,LGRAYBLUE,BLACK,16);
	 }
		
			if(Page_Two)
			{
				if(m)
				{
					LCD_Fill(0,0,LCD_W,LCD_H,BLACK);
					m=m-1;
				}
				 
	      LCD_WR_REG(0x36);
				LCD_WR_DATA8(0xA8);
			}
		else
		{
				if(m)
				{
					LCD_Fill(0,0,LCD_W,LCD_H,BLACK);
					m=m-1;
				}
       	LCD_WR_REG(0x36);
				LCD_WR_DATA8(0x78);
		}
}



